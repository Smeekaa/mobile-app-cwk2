@file:Suppress("NAME_SHADOWING")

import java.io.BufferedReader
import java.io.InputStreamReader
import java.math.RoundingMode
import java.net.URL
import java.text.DecimalFormat
import kotlin.collections.ArrayList
import kotlin.system.exitProcess

// using arrays of data in format double and string
// passed type of data to format (longitude, latitude, depth, magnitude)
// by identifying the length of the string + its range as a numerical double, can determine how to format
// the string in respect to the column sizes on the coursework brief
// returns data as string with spacing
fun formatData(inputString: String, inputDouble: Double, inputType: String): String {

    when (inputType) {
        "lon" -> {
            //  100 to 999
            if (inputString.length == 8 && inputDouble >= 100 && inputType == "lon") {
                print("|  $inputString |")
            }
            // -100 to -999
            else if (inputString.length == 9 && inputDouble <= -100) {
                print("| $inputString |")
            }
            // 10 to 99
            else if (inputString.length == 7 && inputDouble < 100 && inputDouble >= 10) {
                print("|   $inputString |")
            }
            // -10 to -99
            else if (inputString.length == 8 && inputDouble > -100 && inputDouble <= -10) {
                print("|  $inputString |")
            }
            // 0 to 10
            else if (inputString.length == 6 && inputDouble >= 0 && inputDouble < 10) {
                print("|  $inputString |")
            }
            // -1 to -10
            else if (inputString.length == 7 && inputDouble >= -10 && inputDouble < 0) {
                print("|   $inputString |")
            }
        }
        "lat" -> {
            // 100 to 999
            if (inputString.length == 8 && inputDouble >= 100) {
                print(" $inputString  |")
            }
            // -100 -999
            else if (inputString.length == 9 && inputDouble <= -100) {
                print("$inputString  |")
            }
            // 10 to 99
            else if (inputString.length == 7 && inputDouble < 100 && inputDouble >= 10) {
                print("  $inputString  |")
            }
            // -10 to -99
            else if (inputString.length == 8 && inputDouble > -100 && inputDouble <= -10) {
                print(" $inputString  |")
            }
            // 0 to 10
            else if (inputString.length == 6 && inputDouble >= 0 && inputDouble < 10) {
                print("   $inputString  |")
            }
            // -1 to -10
            else if (inputString.length == 7 && inputDouble >= -10 && inputDouble < 0) {
                print("  $inputString  |")
            }
        }
        "depth" -> {
            // 100 to 999
            if (inputString.length == 6 && inputDouble >= 100) {
                print(" $inputString |")
            }
            // 10 to 99
            else if (inputString.length == 5 && inputDouble < 100 && inputDouble >= 10) {
                print("  $inputString |")
            }
            // -10 to -99
            else if (inputString.length == 6 && inputDouble > -100 && inputDouble <= -10) {
                print("  $inputString |")
            }
            // -1 to -10
            else if (inputString.length == 5 && inputDouble >= -10 && inputDouble < 0) {
                print("  $inputString |")
            }
            // 0 to 10
            else if (inputString.length == 4 && inputDouble >= 0 && inputDouble < 10) {
                print("   $inputString |")
            }
        }
        "magnitude" -> {
            // 0 to 10
            if (inputString.length == 3 && inputDouble >= 0 && inputDouble < 10) {
                print(" $inputString |")
            }
            // -1 to -10
            else if (inputString.length == 4 && inputDouble >= -10 && inputDouble < 0) {
                print("  $inputString |")
            }
        }
     }
    return inputString
}

fun main(args: Array<String>) {

    // provides information of what should be supplied as command line arguments
    // checks for launch arguments
    if (args.size < 3) {
        println("must supply at least 5 arguments:")
        println("magnitude(1.0, 2.5, 4.5, all, significant)")
        println("time-period: (hour, day, week, month)")
        println("hemisphere selection: 'north', 'south', 'non'")
        println("depth sorting: 'asc'(ascending), 'dec'(descending), 'non'(none)")
        println("magnitude sorting: 'asc'(ascending), 'dec'(descending), 'non'(none)")
        println("example input: north 1.0 month non non")
        exitProcess(0)
    }

    val hemisphereCheck = args[0]
    val northSouth = ArrayList<String>()
    northSouth.add("north"); northSouth.add("south"); northSouth.add("non")

    val magArgs = args[1]
    val magnitudes = ArrayList<String>()
    magnitudes.add("1.0"); magnitudes.add("2.5"); magnitudes.add("4.5"); magnitudes.add("all"); magnitudes.add("significant")

    val timeArgs = args[2]
    val times = ArrayList<String>()
    times.add("hour"); times.add("day"); times.add("week"); times.add("month")

    val depthAscDscArgs = args[3]
    val depthAscDsc = ArrayList<String>()
    depthAscDsc.add("asc"); depthAscDsc.add("dec"); depthAscDsc.add("non")

    val magsAscDscArgs = args[4]
    val magAscDsc = ArrayList<String>()
    magAscDsc.add("asc"); magAscDsc.add("dec"); magAscDsc.add("non")



    // severity input check booleans
    var magCheck = false; var timeCheck = false; var depthAscDscCheck = false; var magAscDscCheck = false; var northSouthCheck = false

    // checks user input against array of possible commands
    // if not valid input, provide info
    if (magArgs in magnitudes) {
        magCheck = true
    } else println("magnitude: '1.0', '2.5', '4.5', 'all', 'significant'")

    if (timeArgs in times) {
        timeCheck = true
    } else println("time-period: (hour, day, week, month)")

    if (depthAscDscArgs in depthAscDsc) {
        depthAscDscCheck = true
    } else println("depth sorting: 'asc'(ascending), 'dec'(descending), 'non'(none)")

    if (magsAscDscArgs in magAscDsc) {
        magAscDscCheck = true
    } else println("magnitude sorting: 'asc'(ascending), 'dec'(descending), 'non'(none)")

    if (hemisphereCheck in northSouth) {
        northSouthCheck = true
    } else println("hemisphere selection: 'north', 'south', 'non'")

    // continue program is all command line arguments are satisfied
    if (magCheck && timeCheck && depthAscDscCheck && magAscDscCheck && northSouthCheck) {

        val uRL = "https://earthquake.usgs.gov/earthquakes/feed/v1.0/summary/ $magArgs  _  $timeArgs  .csv"
        val urlInput = URL(uRL)
        val input = BufferedReader(InputStreamReader(urlInput.openStream()))
        // used to loop over all input lines
        var inputLine = input.readLine()

        // storing values as double to preform numerical checks when calculating / formatting
        val longitudes = ArrayList<Double>(); val latitudes = ArrayList<Double>(); val depth = ArrayList<Double>(); val magnitudes = ArrayList<Double>()

        // storing same values as strings such that it helps when used with the double equivalent for formatting
        val longString = ArrayList<String>(); val latString = ArrayList<String>(); val depthsString = ArrayList<String>(); val magString = ArrayList<String>()

        // counter used to skip first line of data set (contacting the titles of columns)
        // do while data has next lines, split data, format, add to arrays
        var dataLine = 0
        do {

            val inputSplit = inputLine.split(",".toRegex())

            inputLine = input.readLine()

            if (dataLine >= 1) {

                // relevant data taken from splitting the rows and assigning to variables
                val lats = inputSplit[1].toDouble(); val longs = inputSplit[2].toDouble(); val depths = inputSplit[3].toDouble(); val mags = inputSplit[4].toDouble()

                // method of formatting the doubles
                val formatDepth = DecimalFormat("0.00"); val formatMag = DecimalFormat("0.0"); val formatLongLat = DecimalFormat("###.0000")

                // magnitude + depth have to be rounded up and set to decimal places to maintain accuracy
                val magsRounded = mags.toBigDecimal().setScale(1, RoundingMode.UP).toDouble()
                val depthsRounded = depths.toBigDecimal().setScale(2, RoundingMode.UP).toDouble()

                // perform formatting
                val magFormatted = formatMag.format(mags); val depthFormatted = formatDepth.format(depths); val longFormatted = formatLongLat.format(longs); val latFormatted = formatLongLat.format(lats)


                // add data as well as rounded data to longitudes, latitudes, depth, magnitude
                // add formatted string versions of the data to respected array
                when {
                    args[0] == "non" -> {
//
                        latitudes.add(lats);longitudes.add(longs)
                        depth.add(depthsRounded); depthsString.add(depthFormatted)
                        magnitudes.add(magsRounded); magString.add(magFormatted)
                        longString.add(longFormatted); latString.add(latFormatted)
                    }
                    args[0] == "north"  && lats > 0 -> {
                        latitudes.add(lats);longitudes.add(longs)
                        depth.add(depthsRounded); depthsString.add(depthFormatted)
                        magnitudes.add(magsRounded); magString.add(magFormatted)
                        longString.add(longFormatted); latString.add(latFormatted)
                    }
                    args[0] == "south" && lats < 0 -> {
                        latitudes.add(lats);longitudes.add(longs)
                        depth.add(depthsRounded); depthsString.add(depthFormatted)
                        magnitudes.add(magsRounded); magString.add(magFormatted)
                        longString.add(longFormatted); latString.add(latFormatted)
                    }
                }
            }
            // increment counter (only used to hold current line in data set)
            dataLine += 1

            // preform variable assignment and formatting as there are still lines to process
        } while (inputLine != null)

        // initial formatting
        println("+-----------+-----------+--------+-----+"); println("|    Lon    |    Lat    |  Depth | Mag |"); println("+-----------+-----------+--------+-----+")

        // loop over total elements in longitudes (should be same number of occupancies for all arrays)
        // as for every longitude entry there is a latitudes, magnitude, depth etc...
        // can all be handled in this loop
        // preforms 1 row at a time. Longitude, latitude, depth, magnitude
             for (i in longitudes.indices) {
                formatData(longString[i], longitudes[i], "lon")
                 formatData(latString[i], latitudes[i], "lat")
                 formatData(depthsString[i], depth[i], "depth")
                 formatData(magString[i], magnitudes[i], "magnitude")
                println()
            }
        // final formatting print
        println("+-----------+-----------+--------+-----+")

        // print total occupancies of earthquakes (equal to total lines taken from data)
        val totalQuakes = longitudes.size; println("$totalQuakes quakes")

        // print mean depth with formatting
        val formatMeanDepth = DecimalFormat("0.0"); val meanDepth = formatMeanDepth.format(depth.average())
        println("Mean Depth = $meanDepth km")

        // print mean magnitude with formatting
        val formatMeanMag = DecimalFormat("0.00"); val meanMag = formatMeanMag.format(magnitudes.average())
        println("Mean magnitude = $meanMag")
    }
}
